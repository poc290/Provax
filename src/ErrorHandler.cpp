#include "ErrorHandler.h"

#include <iostream>

#include <xercesc/sax/SAXParseException.hpp>

namespace provax {

void ErrorHandler::warning(const xercesc::SAXParseException & ex)
{
    std::cout << "Avertissement ! Fichier " << xercesc::XMLString::transcode(ex.getSystemId())
              << ", ligne " << ex.getLineNumber() << ", colonne " << ex.getColumnNumber() << " : "
              << xercesc::XMLString::transcode(ex.getMessage()) << std::endl;
}

void ErrorHandler::error(const xercesc::SAXParseException & ex)
{
    std::cout << "Erreur ! Fichier " << xercesc::XMLString::transcode(ex.getSystemId())
              << ", ligne " << ex.getLineNumber() << ", colonne " << ex.getColumnNumber() << " : "
              << xercesc::XMLString::transcode(ex.getMessage()) << std::endl;
}

void ErrorHandler::fatalError(const xercesc::SAXParseException & ex)
{
    std::cout << "Erreur fatale ! Fichier " << xercesc::XMLString::transcode(ex.getSystemId())
              << ", ligne " << ex.getLineNumber() << ", colonne " << ex.getColumnNumber() << " : "
              << xercesc::XMLString::transcode(ex.getMessage()) << std::endl;
}

void ErrorHandler::resetErrors() {}

} // namespace provax
