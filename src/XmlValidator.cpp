#include "XmlValidator.h"

#include <QtCore/QFileInfo>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QPushButton>

#include <xercesc/util/PlatformUtils.hpp>

#include "ErrorHandler.h"

namespace {

const auto RESULT_LABEL_STYLE_SHEET =
    QStringLiteral("border: 1px solid %1; color: %1; padding: 4px;");

const auto DEFAULT_RESULT_LABEL_COLOR = QColor{Qt::gray};
const auto INVALID_RESULT_LABEL_COLOR = QColor{Qt::red};
const auto VALID_RESULT_LABEL_COLOR = QColor{Qt::green};

const auto DEFAULT_RESULT_LABEL_TEXT = QStringLiteral("-");
const auto INVALID_RESULT_LABEL_TEXT = QStringLiteral("KO");
const auto VALID_RESULT_LABEL_TEXT = QStringLiteral("OK");

xercesc::XercesDOMParser * createXmlParser(xercesc::ErrorHandler * handler)
{
    auto * const parser = new xercesc::XercesDOMParser();
    parser->setDoNamespaces(true); // Nécessaire si le drapeau 'do schema' est à `true`
    parser->setDoSchema(true);
    parser->setErrorHandler(handler);
    parser->setValidationConstraintFatal(true);
    parser->setValidationSchemaFullChecking(true);
    parser->setValidationScheme(xercesc::XercesDOMParser::Val_Always);

    return parser;
}

bool loadXsdFile(const QString & filePath, xercesc::XercesDOMParser * parser)
{
    // Un chemin absolu est, semble-t-il, obligatoire pour traiter le schéma XML.
    const auto absolutePath = QFileInfo{filePath}.absoluteFilePath().toStdString();

    auto * const grammar =
        parser->loadGrammar(absolutePath.c_str(), xercesc::Grammar::SchemaGrammarType, true);

    // À ce stade, suite au chargement du fichier XSD, des erreurs peuvent être notifiées au
    // gestionnaire ad hoc.

    const auto isLoaded = (grammar != nullptr);

    if (isLoaded) {
        // https://stackoverflow.com/questions/70712394/no-declaration-found-for-element
        parser->setExternalNoNamespaceSchemaLocation(absolutePath.c_str());
    }

    return isLoaded;
}

bool checkXmlFile(const QString & filePath, xercesc::XercesDOMParser * parser)
{
    // Le parseur est validant, c'est-à-dire que le contrôle d'un document XML à l'aide d'un schéma
    // est réalisé conjointement à l'extraction de ses données.
    parser->parse(filePath.toStdString().c_str());

    return (parser->getErrorCount() == 0);
}

} // namespace

namespace provax {

XmlValidator::XmlValidator()
    : m_ValidateButton{new QPushButton{QStringLiteral("Valider")}},
      m_ResultLabel{new QLabel},
      m_ErrorHandler{new ErrorHandler}
{
    // AFAIRE : désactiver le bouton "Valider" par défaut, puis l'activer si le parseur XML est
    // initialisé et si les chemins d'accès désignent des fichiers existants

    connect(m_ValidateButton, &QPushButton::clicked, this, &XmlValidator::validate);

    m_ResultLabel->setStyleSheet(RESULT_LABEL_STYLE_SHEET.arg(DEFAULT_RESULT_LABEL_COLOR.name()));
    m_ResultLabel->setText(DEFAULT_RESULT_LABEL_TEXT);

    try {
        // L'API Xerces doit être initialisée avant d'être utilisée.
        xercesc::XMLPlatformUtils::Initialize();

        m_XmlParser.reset(createXmlParser(m_ErrorHandler.get()));
    }
    catch (const xercesc::XMLException & ex) {
        Q_UNUSED(ex)
    }
}

XmlValidator::~XmlValidator() { xercesc::XMLPlatformUtils::Terminate(); }

void XmlValidator::position(QGridLayout * layout, int fromRow) const
{
    layout->addWidget(m_ValidateButton, fromRow, 0, 1, layout->columnCount());
    layout->addWidget(m_ResultLabel, (fromRow + 1), 0, 1, layout->columnCount());
}

void XmlValidator::setXsdFilePath(const QString & path) { m_XsdFilePath = path; }
void XmlValidator::setXmlFilePath(const QString & path) { m_XmlFilePath = path; }

void XmlValidator::validate() const
{
    auto isValid = false;

    if (loadXsdFile(m_XsdFilePath, m_XmlParser.get())) {
        isValid = checkXmlFile(m_XmlFilePath, m_XmlParser.get());
    }

    m_ResultLabel->setStyleSheet(RESULT_LABEL_STYLE_SHEET.arg(
        isValid ? VALID_RESULT_LABEL_COLOR.name() : INVALID_RESULT_LABEL_COLOR.name()));

    // AFAIRE : le cas échéant, afficher une description de l'erreur détectée
    m_ResultLabel->setText(isValid ? VALID_RESULT_LABEL_TEXT : INVALID_RESULT_LABEL_TEXT);
}

} // namespace provax
