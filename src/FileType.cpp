#include "FileType.h"

namespace provax {

FileType::FileType(const QString & name, const QString & extension)
    : m_Name{name}, m_Extension{extension}
{
}

QString FileType::getName() const { return m_Name; }
QString FileType::getExtension() const { return m_Extension; }

} // namespace provax
