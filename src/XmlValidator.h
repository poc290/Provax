#ifndef PROVAX_XMLVALIDATOR_H
#define PROVAX_XMLVALIDATOR_H

#include <xercesc/parsers/XercesDOMParser.hpp>

#include "Control.h"

class QLabel;
class QPushButton;

namespace provax {

class ErrorHandler;

class XmlValidator : public Control {
    Q_OBJECT

public:
    XmlValidator();

    ~XmlValidator();

    void position(QGridLayout * layout, int fromRow) const override;

public slots:
    void setXsdFilePath(const QString & path);
    void setXmlFilePath(const QString & path);

private slots:
    void validate() const;

private:
    QPushButton * const m_ValidateButton;
    QLabel * const m_ResultLabel;
    QScopedPointer<xercesc::XercesDOMParser> m_XmlParser;
    const QScopedPointer<ErrorHandler> m_ErrorHandler;
    QString m_XsdFilePath;
    QString m_XmlFilePath;
};

} // namespace provax

#endif // PROVAX_XMLVALIDATOR_H
