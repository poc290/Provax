#ifndef PROVAX_CONTROL_H
#define PROVAX_CONTROL_H

#include <QtCore/QObject>

class QGridLayout;

namespace provax {

// Classe représentant un composant de l'interface graphique
class Control : public QObject {
public:
    Control() = default;

    virtual ~Control() = default;

    // Méthode de positionnement du composant dans un `QGridLayout`
    virtual void position(QGridLayout * layout, int fromRow) const = 0;
};

} // namespace provax

#endif // PROVAX_CONTROL_H
