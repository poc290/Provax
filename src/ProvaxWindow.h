#ifndef PROVAX_PROVAXWINDOW_H
#define PROVAX_PROVAXWINDOW_H

#include <QtWidgets/QWidget>

namespace provax {

class FileSelector;
class XmlValidator;

class ProvaxWindow : public QWidget {
public:
    explicit ProvaxWindow(QWidget * parent = nullptr);

private:
    FileSelector * const m_XsdFileSelector;
    FileSelector * const m_XmlFileSelector;
    XmlValidator * const m_XmlValidator;
};

} // namespace provax

#endif // PROVAX_PROVAXWINDOW_H
