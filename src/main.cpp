#include <QtGui/QIcon>
#include <QtWidgets/QApplication>

#include "ProvaxWindow.h"

int main(int argc, char * argv[])
{
    QApplication app{argc, argv};
    app.setApplicationName(QStringLiteral("Provax"));
    app.setWindowIcon(QIcon{QStringLiteral(":/icons/provax.ico")});

    provax::ProvaxWindow window;
    window.show();

    return app.exec();
}
