#include "ProvaxWindow.h"

#include <QtWidgets/QGridLayout>

#include "FileSelector.h"
#include "XmlValidator.h"

namespace {

const auto XSD_FILE_TYPE = provax::FileType{QStringLiteral("XSD"), QStringLiteral("*.xsd")};
const auto XML_FILE_TYPE = provax::FileType{QStringLiteral("XML"), QStringLiteral("*.xml")};

} // namespace

namespace provax {

ProvaxWindow::ProvaxWindow(QWidget * parent)
    : QWidget{parent},
      m_XsdFileSelector{new FileSelector{XSD_FILE_TYPE}},
      m_XmlFileSelector{new FileSelector{XML_FILE_TYPE}},
      m_XmlValidator{new XmlValidator}
{
    connect(m_XsdFileSelector,
            &FileSelector::selectionChanged,
            m_XmlValidator,
            &XmlValidator::setXsdFilePath);
    connect(m_XmlFileSelector,
            &FileSelector::selectionChanged,
            m_XmlValidator,
            &XmlValidator::setXmlFilePath);

    auto * const layout = new QGridLayout;
    m_XsdFileSelector->position(layout, 0);
    m_XmlFileSelector->position(layout, 1);
    m_XmlValidator->position(layout, 2);
    setLayout(layout);
}

} // namespace provax
