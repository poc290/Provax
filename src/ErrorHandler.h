#ifndef PROVAX_ERRORHANDLER_H
#define PROVAX_ERRORHANDLER_H

#include <xercesc/sax/ErrorHandler.hpp>

namespace provax {

class ErrorHandler : public xercesc::ErrorHandler {
public:
    ErrorHandler() = default;

    virtual void warning(const xercesc::SAXParseException & ex) override;
    virtual void error(const xercesc::SAXParseException & ex) override;
    virtual void fatalError(const xercesc::SAXParseException & ex) override;
    virtual void resetErrors() override;
};

} // namespace provax

#endif // PROVAX_ERRORHANDLER_H
