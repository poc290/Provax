#include "FileSelector.h"

#include <QtWidgets/QFileDialog>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QLabel>
#include <QtWidgets/QLineEdit>
#include <QtWidgets/QPushButton>

namespace provax {

FileSelector::FileSelector(const FileType & type)
    : m_FileType{type},
      m_Label{new QLabel{QString{"Fichier %1 :"}.arg(m_FileType.getName())}},
      m_TextField{new QLineEdit},
      m_BrowseButton{new QPushButton{QStringLiteral("Parcourir...")}}
{
    m_TextField->setMinimumWidth(250);

    // AFAIRE : vérifier que le chemin renseigné pointe vers un fichier existant

    connect(m_TextField, &QLineEdit::textChanged, this, &FileSelector::selectionChanged);
    connect(m_BrowseButton, &QPushButton::clicked, this, &FileSelector::browse);
}

void FileSelector::position(QGridLayout * layout, int fromRow) const
{
    layout->addWidget(m_Label, fromRow, 0);
    layout->addWidget(m_TextField, fromRow, 1);
    layout->addWidget(m_BrowseButton, fromRow, 2);
}

void FileSelector::browse() const
{
    const auto path = QFileDialog::getOpenFileName(
        nullptr,
        QStringLiteral("Sélectionner un fichier"),
        QString{PROVAX_DATA_PATH},
        QString{"Fichiers %1 (%2)"}.arg(m_FileType.getName(), m_FileType.getExtension()));

    // Si l'utilisateur clique sur le bouton "Annuler", alors une chaîne de caractères vide est
    // retournée.
    if (!path.isEmpty()) {
        m_TextField->setText(path);
    }
}

} // namespace provax
