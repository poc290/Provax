#ifndef PROVAX_FILESELECTOR_H
#define PROVAX_FILESELECTOR_H

#include "Control.h"
#include "FileType.h"

class QLabel;
class QLineEdit;
class QPushButton;

namespace provax {

class FileSelector : public Control {
    Q_OBJECT

public:
    explicit FileSelector(const FileType & type);

    void position(QGridLayout * layout, int fromRow) const override;

signals:
    void selectionChanged(const QString & filePath);

private slots:
    void browse() const;

private:
    const FileType m_FileType;
    QLabel * const m_Label;
    QLineEdit * const m_TextField;
    QPushButton * const m_BrowseButton;
};

} // namespace provax

#endif // PROVAX_FILESELECTOR_H
