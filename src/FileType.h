#ifndef PROVAX_FILETYPE_H
#define PROVAX_FILETYPE_H

#include <QtCore/QString>

namespace provax {

class FileType {
public:
    FileType(const QString & name, const QString & extension);

    QString getName() const;
    QString getExtension() const;

private:
    const QString m_Name;
    const QString m_Extension;
};

} // namespace provax

#endif // PROVAX_FILETYPE_H
