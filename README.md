# Provax

Provax est un validateur XML, c'est-à-dire qu'il permet de vérifier la conformité d'un document XML à une spécification au format XSD (*XML Schema Definition*). Cette tâche est accomplie à l'aide de la bibliothèque [Xerces-C++](https://xerces.apache.org/xerces-c/).

## Licence

[MIT](https://choosealicense.com/licenses/mit/)
