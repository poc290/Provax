find_program(PROVAX_CLANGFORMAT_EXE
             NAMES clang-format
             PATHS "C:\\Program Files\\LLVM"
             PATH_SUFFIXES bin)

if(PROVAX_CLANGFORMAT_EXE)
    message(STATUS "clang-format found: ${PROVAX_CLANGFORMAT_EXE}")

    add_custom_target(format ALL
                      COMMAND ${PROVAX_CLANGFORMAT_EXE} -i -style=file ${PROVAX_SRC_FILES}
                      WORKING_DIRECTORY "${CMAKE_SOURCE_DIR}"
                      COMMENT "Formatting code..."
                      VERBATIM)
endif()
