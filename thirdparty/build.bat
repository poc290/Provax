@echo off

set _7ZIP_HOME="C:\Program Files\7-Zip"
set _MINGW_HOME=C:\Qt\5.12.10\Tools\mingw730_32
set _CMAKE_HOME=C:\Qt\5.12.10\Tools\CMake_64

set PATH=%_7ZIP_HOME%;%_MINGW_HOME%\bin;%_CMAKE_HOME%\bin;%PATH%

set _XERCES_VERSION=3.2.4
set _XERCES_SRC_DIR=xerces

if exist %_XERCES_SRC_DIR% rmdir /S /Q %_XERCES_SRC_DIR%
7z e xerces-c-%_XERCES_VERSION%.tar.xz && 7z x xerces-c-%_XERCES_VERSION%.tar && rename xerces-c-%_XERCES_VERSION% %_XERCES_SRC_DIR%
del *.tar

cd %_XERCES_SRC_DIR% && mkdir build && cd build

cmake -G "MinGW Makefiles" -DCMAKE_INSTALL_PREFIX=./install -DCMAKE_BUILD_TYPE=Release -DBUILD_SHARED_LIBS=OFF ..

mingw32-make
mingw32-make install

pause