cmake_minimum_required(VERSION 3.24)

project(Provax)

set(CMAKE_AUTOMOC ON)
set(CMAKE_AUTORCC ON)

list(APPEND CMAKE_PREFIX_PATH "${CMAKE_SOURCE_DIR}/thirdparty/xerces/build/install")

find_package(Qt5 COMPONENTS Widgets REQUIRED)
find_package(XercesC 3.2 EXACT REQUIRED)

set(PROVAX_SRC_FILES src/main.cpp
                     src/ProvaxWindow.cpp
                     src/ProvaxWindow.h
                     src/FileType.cpp
                     src/FileType.h
                     src/Control.h
                     src/FileSelector.cpp
                     src/FileSelector.h
                     src/XmlValidator.cpp
                     src/XmlValidator.h
                     src/ErrorHandler.cpp
                     src/ErrorHandler.h)

add_executable(provax ${PROVAX_SRC_FILES}
                      resources/provax.qrc)

target_link_libraries(provax Qt5::Widgets
                             XercesC::XercesC)

target_compile_definitions(provax PUBLIC PROVAX_DATA_PATH="${CMAKE_SOURCE_DIR}/data")

include(cmake/ProvaxFormatCode.cmake)
