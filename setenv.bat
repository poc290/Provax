@echo off

set _QT_VERSION=5.12.10

set _QT_DIR=C:\Qt\%_QT_VERSION%
set _QT_TOOLS_DIR=%_QT_DIR%\Tools

set _QT_HOME=%_QT_DIR%\%_QT_VERSION%\mingw73_32
set _MINGW_HOME=%_QT_TOOLS_DIR%\mingw730_32
set _CMAKE_HOME=%_QT_TOOLS_DIR%\CMake_64
set _QT_CREATOR_HOME=%_QT_TOOLS_DIR%\QtCreator

set PATH=%_QT_HOME%\bin;%_MINGW_HOME%\bin;%_CMAKE_HOME%\bin;%_QT_CREATOR_HOME%\bin;%PATH%

cmd /K